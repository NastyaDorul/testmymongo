const MongoClient = require("mongodb").MongoClient;

const { uri, db } = require("./config");

const ObjectId = require("mongodb").ObjectId;

const testConnection = async () => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  console.log("connected");
  client.close();
};

const deleteNote = async note => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  const usersCollection = await client.db(db).collection("step-project");
  await usersCollection.deleteOne(note);
  console.log("deleted one note");
  client.close();
};

let noteId = "";

const addNote = async note => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  const usersCollection = await client.db(db).collection("step-project");
  await usersCollection.insertOne(note);
  console.log("1 Note inserted");
  noteId = note._id;
  console.log(data, noteId);
  client.close();
};

const getNote = async () => {
  const client = new MongoClient(uri, { useNewUrlParser: true });
  await client.connect();
  const usersCollection = await client.db(db).collection("step-project");
  const data = await usersCollection.find({}).toArray();
  console.log("1 Note got");
  console.log(data);
  client.close();
  return data;
};
//const findNote = async () => {
//const client = new MongoClient(uri, { useNewUrlParser: true });
//await client.connect();
//const usersCollection = await client.db(db).collection("step-project");
//const docs = await usersCollection.find({ firstNote: "Mariia" }).toArray();
//const docs = await usersCollection.find({ _id: req.params.id }).toArray();
//const docs = await usersCollection
//.find(elem => {
// elem === noteId;
//})
//.toArray();
//console.log(docs);
//client.close();
//return docs;
//};

module.exports = {
  testConnection,
  deleteNote,
  addNote,
  getNote
  //findNote
};
