//-----------------------------------------файлы подключения---------------------------------------------------
const express = require("express");
const app = express();
const port = 3000;
const db = require("./db/"); //что именно тут подключено
const Note = require("./app/moduls/noteController"); ///с роутами разобраться
const bodyParser = require("body-parser");
app.use("/assets", express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// set the view engine to ejs
app.set("view engine", "ejs"); // use res.render to load up an ejs view file
//------------------------------------------------------------------------------------------------------------
Note(app, db);
//-----------------------------------гет-запросы-------------------------------------------------------------------------
// home page
app.get("/", function(req, res) {
  // get-запрос отображение страницы индекс
  res.render("pages/index");
});


//app.get('/note/:id', async function(req, res) {
//  const newnotes = {
//    id:newnotes.id,
//  data:newnotes.data
//}
//  await db.getNote(newnotes);
//res.render('pages/editNote',{
//  newnotes
//});
//});

app.listen(port, () => {
  console.log("We are live on " + port);
});
