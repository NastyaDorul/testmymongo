const ObjectId = require("mongodb").ObjectId;

module.exports = function(app, db) {
  app.get("/note/:id", async function(req, res) {
    // get-запрос получение id из get-строки

    const query = { _id: ObjectId(req.params.id) };

    let result = null;
    try {
      result = await usersCollection.findOne({ query }); //toArray()
      //result = await db.collection("to-do-list").findOne(query);
    } catch (err) {
      if (result) console.log(err);
    }
    const showData = {
      id: query._id,
      title: result.title
    };
    res.send({ notes: showData }); //render("updateNote", { notes: showData });
  });
  app.put("/notes/:id", async (req, res) => {
    console.log("PUT work");
    const query = { _id: ObjectId(req.params.id) };
    const newData = {
      _id: query._id,
      title: req.body.title
    };
    let result = null;
    try {
      result = await db
        .collection("to-do-list")
        .updateOne(query, { $set: newData }, { upsert: true });
    } catch (err) {
      console.log(err);
    }
    res.send({ notes: showData });
    //res.render("updateNote", { notes: newData });
  });
};
