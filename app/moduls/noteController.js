module.exports = function(app, db) {
  app.get("/note", async function(req, res) {
    // get-запрос получение данных из базы, отображение на заметке

    const notes = await db.getNote();
    //await db.deleteNote();
    res.render("pages/note", {
      notes
    });
  });

  app.post("/note", async function(req, res) {
    const note = {
      title: req.body.title
      //description: req.body.description
    };
    await db.addNote(note);
    res.redirect("/note");
    res.end();
  });

  //app.delete("/note/:id", async function(req, res) {
  //console.log("delete work");

  // const result = await db.findNote();
  // });
};

/*function NoteForm(options) {
  var elem = options.elem;

  elem.onmousedown = function() {
    return false;
  };

  elem.onclick = function(event) {
    if (event.target.closest("form")) {
      elem.classList.toggle("color");
    }
  };
}

module.exports = {
  NoteForm
};*/
